
def even(n):
    return n/2

def odd(n):
    return 3*n + 1

def collatz(a, b):
    acount = 0
    bcount = 0
    if a == b:
        return 'equal'
    
    while(a != 1):
        if a % 2 == 0: a = even(a)
        else: a = odd(a)
        acount += 1

    while(b != 1):
        if b % 2 == 0: b = even(b)
        else: b = odd(b)
        bcount += 1
        
    if acount < bcount:
        return 'a'

    else:
        return 'b'

