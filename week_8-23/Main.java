package luhn_test;
import java.util.ArrayList;


public class Main {
	public static boolean testCard(String num) {
		ArrayList<Integer> doubled = new ArrayList<Integer>();
		StringBuilder card = new StringBuilder(String.valueOf(num));
		
		if (card.length() < 14 || card.length() > 19) {
			return false;
		}
		int test = Character.getNumericValue(card.charAt(card.length()-1));
		card.deleteCharAt(card.length()-1);
		card.reverse();
		for(int i = 0; i < card.length(); i++) {
			if((i+1)%2 != 0) {
				int high = Character.getNumericValue(card.charAt(i))*2;
					if(high > 9) {
						String temp = Integer.toString(high);
						high = Character.getNumericValue(temp.charAt(0)) + Character.getNumericValue(temp.charAt(1));
						doubled.add(high);
					}
					else {doubled.add(high);}
			}
			else {doubled.add(Character.getNumericValue(card.charAt(i)));}
		}
		int sum = 0;
		for(int i = 0; i < doubled.size(); i++) {
			sum += doubled.get(i);
		}
		String last = String.valueOf(sum);
		if(10 - Character.getNumericValue(last.charAt(1)) == test) {
			return true;
		}
		return false;
	}
	public static void main(String[] args) {
		String testNum = "1234567890123452";
		System.out.print(testCard(testNum));
		
	}

}
